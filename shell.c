#include<stdio.h>
#include<readline/readline.h>
#include<readline/history.h>

#include<stdlib.h>
#include<string.h>

#include<sys/wait.h>
#include<unistd.h>


int main()
{
	/* maximum number of commands we support is 50*/
	int MAXCOMMANDS = 50;
	char* input = NULL;
	char* command = NULL;
	char* commandList[MAXCOMMANDS];
	int i = 0;
	int j = 0;
	int commandCount = 0;
	int pipes[MAXCOMMANDS][2];
	char* argv[MAXCOMMANDS];
	int argCount = 0;
	char* arg = NULL;
	int flag = 0;
	pid_t pid;
	char path[30] = "/usr/bin/";
	char* environ[] = {NULL};
	while (1) {
		input = readline("");
		// store all the commands in the memory
		command = strtok(input,"|");
		i = 0;
		while (command != NULL) {
			commandList[i] = (char*)malloc(sizeof(char)*(strlen(command)+1));
			strcpy(commandList[i],command);
			i++;
			command = strtok(NULL, "|");
		}
		commandList[i] = NULL;
		commandCount = i;
		free(input);

		// create all the pipes we need
		for(i=0;i<commandCount-1;i++) {
			if (pipe(pipes[i])==-1) {
				perror("pipe");
				exit(EXIT_FAILURE);
			}
		}

		// parse each command seperately
		for (i=0;i<commandCount;i++) {
			command = commandList[i];
			arg = strtok(command, " ");
			j = 0;
			while(arg != NULL) {
				argv[j] = (char*)malloc(sizeof(char)*(strlen(arg)+1));
				strcpy(argv[j],arg);
				j++;
				arg = strtok(NULL," ");
			}
			argCount = j;
			argv[j] = NULL;
			pid = fork();
			if (pid == -1) {
				perror("fork");
				exit(EXIT_FAILURE);
			}else if (pid == 0) {
				// mapp stdio to the pipes, except for the first process and the last process
				if (i<commandCount - 1) {
					if(dup2(pipes[i][1],STDOUT_FILENO) == -1) {
						perror("dup2");
						exit(EXIT_FAILURE);
					}
				}
				if (i>0) {
					if(dup2(pipes[i-1][0],STDIN_FILENO) == -1) {
						perror("dup2");
						exit(EXIT_FAILURE);
					}
				}
				strcat(path,argv[0]);
				flag = execve(path,argv,environ);
				if (flag < 0) {
					perror("execve");
					exit(EXIT_FAILURE);
				}
			}
			wait(NULL);
			if (i<commandCount-1) {
				close(pipes[i][1]);
			}
			if (i>0) {
				close(pipes[i-1][0]);
			}

			for (j=0;j<argCount;j++) {
				free(argv[j]);
			}
		}

		for (i=0;i<commandCount;i++) {
			free(commandList[i]);
		}
	}
}
